from django.db import models

class Compile(models.Model):
    command = models.CharField(verbose_name='Command', max_length=500, blank=False, null=False)


class Run(models.Model):
    params = models.CharField(verbose_name='Parameters', max_length=500, blank=False, null=False)


class Template(models.Model):
    description = models.TextField(verbose_name='Description', blank=False, null=False)
    title = models.CharField(verbose_name='Title', max_length=500, blank=False, null=False)


class Code(models.Model):
    code = models.TextField(verbose_name='Code', blank=False, null=False)
    comments = models.TextField(verbose_name='Comments', blank=False, null=False)
    is_template = models.BooleanField(default=False)
    template = models.ForeignKey(Template)
    title = models.CharField(verbose_name='Title', max_length=500, blank=False, null=False)
    