# -*- coding: utf-8 -*-

from django.core.context_processors import csrf
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext

def index(request):
    print 'index'
    c = {'message': 'hello'}
    return render_to_response('frontend/index.html', c, context_instance=RequestContext(request))
